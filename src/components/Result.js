import React from "react";
import { DataGrid } from "@mui/x-data-grid";

const columns = [
  { field: "avatar_url", headerName: "AvatarUrl", width: 620, sortable: false },
  { field: "login", headerName: "Login", width: 240 },
  { field: "type", headerName: "Type", width: 240, sortable: false },
];

function Result(props) {
  const rows = props.info;
  return (
    <div className="container">
      <div className="row" style={{ marginTop: "20px" }}>
        <div className="col-md-12">
          <div style={{ height: 582, width: "100%" }}>
            <DataGrid
              style={{
                fontSize: "17px",
                borderRadius: "10px",
                border: "2px solid lightblue",
              }}
              rows={rows}
              columns={columns}
              pageSize={9}
              rowsPerPageOptions={[9]}
              disableSelectionOnClick
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Result;
