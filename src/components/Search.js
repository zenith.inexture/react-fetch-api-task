import React from "react";
import Result from "./Result";

function Search() {
  const [login, setLogin] = React.useState("");
  const [data, setData] = React.useState([]);
  const [error, setError] = React.useState("");

  const handlesubmit = () => {
    const fetchdata = async () => {
      await fetch(`https://api.github.com/search/users?q=${login}`)
        .then((response) => response.json())
        .then((data) => setData(data.items))
        .catch((error) => setError("Error..."));
    };
    fetchdata();
  };
  return (
    <div className="App">
      <label style={{ fontSize: "20px", color: "blue" }}>
        Login :
        <input
          type="text"
          onChange={(e) => setLogin(e.target.value)}
          value={login}
          className="login_input"
        ></input>
      </label>
      <button onClick={handlesubmit} className="submit_btn">
        Submit
      </button>
      {error === "" && data.length !== 0 ? (
        <Result info={data} />
      ) : (
        <h2 style={{ color: "red" }}>{error}</h2>
      )}
    </div>
  );
}

export default Search;
